# Bedienungsanleitung zur Excel-Tabelle "Messstellenliste.xlsx"

[[_TOC_]]

## Einführung

Dieses Dokument enthält eine Bedienungsanleitung zur Excel-Tabelle "Messstellenliste.xlsx". Die Vorgehensweisen zur [Messstellendokumentation](#messstellendokumentation) und zur [Berechnung der Messunsicherheit](#berechnung-messunsicherheit) werden erklärt. Ferner wird auf die Berechnung der Messunsicherheit sowie auf die Implementierung der Berechnung eingegangen.

Die Excel-Tabelle ist außerdem als Schnittstelle zur Messstellendokumentation zu sehen. Das Python-Skript `seriendruck.py` extrahiert tabellarisch vorliegende Informationen zu den einzelnen Messtellen und überträgt sie in entsprechend viele Kopien der `Vorlage_Messstellendokumentation.docx`, die mit den Namen der Messstellen benannt werden.

Die Messstellenliste besteht aus 7 Arbeitsblättern:

* Projekt - allgemeine Informationen zu dem jeweiligen Projekt
* Anleitung - Kurzanleitung für die Bedienung der Messstellenliste
* Messstellenliste
* Berechnung Messunsicherheit - Eingabemöglichkeit für die messstellen-spezifischen Angaben zur Messunsicherheitsberechnung sowie Ergebnisse der Unsicherheitsberechnung
* Datenblätter Sensor - detaillierte Informationen zu den verwendeten Sensoren
* Datenblätter Messverstärker - detaillierte Informationen zu den verwendeten Messverstärkern

### Begriffserklärung

| Begriff | Definition |
| :--- | :--- |
| Full Scale | Der maximal mögliche Messbereich eines Sensors. Der tatsächlich auftretende Messbereich wird mit $`\Delta Y`$ gekennzeichnet. Die Full Scale fängt oft (aber nicht immer!) bei null an, und endet mit dem "Endwert" C, auch "Kennwert" genannt. Außerdem werden häufig die englischen Bezeichnungen "full span" (= "full scale") sowie "reading" bzw. "percentage of reading" (= Messwert) verwendet. |
| Standard-Messunsicherheit | Intervall, in dem der wahre Wert der Messgröße mit einer Wahrscheinlichkeit von ca. 68 % enthalten ist. Angelehnt an die Standardabweichung entspricht die Standard-Messunsicherheit dem Intervall $`\pm \sigma`$ einer Gauss-Verteilung. |
| Erweiterte Messunsicherheit | Intervall, in dem der wahre Wert der Messgrößen mit einer Wahrscheinlichkeit von ca. 95 % enthalten ist. Das Intervall ist doppelt so breit wie die Standard-Messunsicherheit (Erweiterungsfaktor k = 2) |

## Messstellendokumentation

Auf Basis der Messstellenliste kann für jede Messstelle eine vorausgefüllte Dokumentation erstellt werden, die noch mit weiteren spezifischen Informationen ergänzt werden muss.
Das Skript `seriendruck.py` liest hierzu die Messstellenliste und erzeugt eine individualisierte Kopie der "Vorlage_Messstellendokumentation.docx" für jede Zeile der Messstellenliste, in der die Spalten "Messstellenname" und "Signalart" ausgefüllt sind.
Ist die Signalart nicht bekannt, so ist aus dem Drop-Down-Menü die Option "unbekannt" auszuwählen.

Grundsätzlich gilt: die orangenen Felder sind auszufüllen, während die grau hinterlegten automatisch aus dem Arbeitsblatt "Datenblätter Sensor" übernommen werden.
Damit diese Daten übernommen werden können, ist es notwendig, das Arbeitsblatt "Datenblätter Sensor" mit Informationen zu bestücken.
Hierbei sind für die Messstellendokumentation in erster Linie die "Allgemeinen Angaben" wichtig (Spalten A bis F).
Die weiteren Daten sind vor allem für die Berechnung der Messunsicherheit relevant.

Soll also eine Messstellendokumentation erstellt werden, so ist folgendermaßen vorzugehen:

* In der Excel-Tabelle "Messstellenliste.xlsx":
  * Im Arbeitsblatt "Datenblätter Sensor" die Daten der verwendeten Sensorarten eintragen (für die Dokumentation sind dabei vor allem die "Allgemeinen Angaben" relevant, also Spalten A bis F)
  * Im Arbeitsblatt "Messstellenliste" innerhalb der Spalte "Messstellenname" die Messstellennamen eingeben, sowie in den Spalten "Sensorart" und "Signalart" aus dem Drop-Down-Menü den entsprechenden Sensor und die Signalart wählen (das Drop-Down-Menü stellt Werte aus dem Arbeitsblatt "Datenblätter Sensor" zur Verfügung; die grau hinterlegten Felder werden automatisch übernommen)
  * Die Messverstärker sind für die Sensordokumentation irrelevant und werden nur für die Berechnung der Messunsicherheit verwendet. Der Vollständigkeit halber können sie aber im Arbeitsblatt "Messstellenliste" auch über ein Drop-Down-Menü ausgewählt werden, hierfür ist ggf. vorher das Arbeitsblatt "Datenblätter Messverstärker" mit Informationen zu bestücken.
* Anaconda Prompt oder Windows CMD öffnen, zum Ordner "Seriendruck Messstellendokumentation" navigieren
* `seriendruck.py` ausführen: `python seriendruck.py <pfad/zur/messstellenliste.xlsx\>`
  * Es wird nach dem Projektnamen gefragt: Projektnamen eingeben
  * Es wird nach dem Autor gefragt: Name des Autors eingeben
  * Die angelegten Dokumentationen werden auf dem Arbeitsverzeichnis im Ordner "output" gespeichert
* Damit steht das Grundgerüst der Dokumentation. Für jede Messstelle ist ein Worddokument mit den allgemeinen Daten und der entsprechenden Formatierung angelegt. Diese Worddokumente müssen jetzt noch händisch mit den fehlenden Informationen bestückt werden.

## Berechnung Messunsicherheit

Soll eine Berechnung der Messunsicherheit durchgeführt werden, so ist folgenderweise vorzugehen:

* Die Arbeitsblätter "Datenblätter Sensor" und "Datenblätter Messverstärker" müssen mit Informationen bestückt werden.
* Im Arbeitsblatt "Messstellenliste" müssen die Messstellennamen eingetragen, sowie aus den Drop-Down-Menüs der Spalten "Messverstärker" und "Sensorart" die entsprechend zutreffenden Optionen ausgewählt werden. Ist der Messverstärker nicht bekannt, bzw. soll er nicht mit in die Berechnung der Unsicherheit einfließen, so wird aus dem Drop-Down-Menü die Option "unbekannt" ausgewählt.
* Die orangenen Felder im Arbeitsblatt "Berechnung" müssen nach Möglichkeit ausgefüllt werden.
* Dabei gilt sowohl für die Daten in den Arbeitsblättern "Datenblätter", als auch in "Berechnung", dass nicht zwingend alle Daten eingetragen werden müssen, sondern nur die, die vorliegen und entsprechend in der Unsicherheitsberechnung berücksichtigt werden sollen. Zwingend erforderlich sind:
  * Die Full Scale (vollständiger Messbereich, FS) für die Sensoren in der Tabelle "Datenblätter Sensor" (vor allem, wenn Fehler in Abhängigkeit der Full Scale vorliegen!)
  * Der Nenntemperaturbereich (T<sub>min</sub> - T<sub>max</sub>) für Sensor und Messverstärker, sofern nicht die wirkliche Arbeitstemperatur und die Referenztemperatur bekannt sind (Arbeitstemperatur und Referenztemperatur finden sich im Arbeitsblatt "Berechnung Messunsicherheit").
* Wurden die Datenblätter korrekt mit Informationen bestückt sowie in der Messstellenliste die Sensoren ausgewählt, so kann im Datenblatt "Berechnung Messunsicherheit" die Gesamtunsicherheit für die jeweilige Messstelle abgelesen werden. Diese wird abhängig von den eingetragenen Informationen entweder als absolute Zahl dargestellt, oder (falls keine Angabe zum Messwert $`Y`$ bzw. Messwertebereich $`\Delta Y`$ gemacht wurde) in Abhängigkeit des Messwertes $`Y`$ bzw. Messwertbereiches $`\Delta Y`$ (s. [Berechnung Gesamtunsicherheit](#berechnung-gesamtunsicherheit)).

## Arbeitsblatt "Datenblätter Sensor"

In dem Blatt "Datenblätter Sensor" werden alle bekannten Daten zu den Sensorarten festgehalten. Diese sind aufgeteilt in allgemeine Angaben, absolute Fehler, Fehler in Abhängigkeit der Full Scale FS, Fehler in Abhängigkeit des Messwerts Y, und Fehler bezogen auf den Messbereich. Die Daten werden dabei in einer sich selbst erweiternden Tabelle festgehalten.

Wichtig hierbei ist, dass nicht alle Felder zwingend ausgefüllt werden müssen. Die Berechnung funktioniert auch dann, wenn nicht alle Felder ausgefüllt sind. Worauf allerdings geachtet werden sollte ist, dass ein Wert bei der Full Scale eingetragen wird. Wenn ein Temperaturfehler vorliegt, dann sollte auch die Arbeitstemperaturspanne, also die Werte T<sub>min</sub> und T<sub>max</sub> angegeben werden, sofern nicht die wirkliche Arbeitstemperatur und die Referenztemperatur bekannt sind.

Die Fehlerwerte werden folgendermaßen angegeben:

```math
\frac{\text{Wert} \cdot \text{Verteilungsfaktor}}{\text{Faktor der Erweiterung} \cdot \text{Halbbreite}}
```

Erklärungen zu den Parametern der Fehlerwert-Berechnung:

| Parameter | Erklärung |
| :--- | :--- |
| $`\text{Wert}`$ | Im Datenblatt angegebener Fehlerwert |
| $`\text{Verteilungsfaktor}`$ | Je nach Verteilung unterscheidet sich dieser Faktor, siehe [Verteilungsfunktion](#verteilungsfunktion). Ist kein Wert angegeben, so wird davon ausgegangen, dass jedes Gerät die angegebenen Grenzen einhält, und es wird von einer Verteilung mit festen oberen und unteren Grenzen ausgegangen (Rechteckverteilung) [^lit-001] [^lit-002] |
| $`\text{Faktor der Erweiterung}`$ | Manche Hersteller geben in ihren Datenblättern erweiterte Messunsicherheiten an, diese Erweiterung muss wieder herausgerechnet werden. Ist dieser Wert nicht extra angegeben, so wird mit $`k=2`$ gearbeitet [^lit-002] |
| $`\text{Halbbreite}`$ | Dies ist die halbe Breite der Verteilung des im Datenblatt angegebenen Fehlerwertes. Ist ein "fester Wert" gegeben, so nimmt die Halbbreite den Wert 2 an (der Fehlerwert wird also durch 2 geteilt). Ist der Fehlerwert im Datenblatt als $`±`$-Wert angegeben, so bezeichnet nimmt die Halbbreite den Wert 1 an. |

### Verteilungsfunktion

Je nach Verteilungsfunktion ergeben sich unterschiedliche Verteilungsfaktoren. Wird im Datenblatt von einem "typischen" Wert gesprochen, so ist die Normalverteilung gemeint. Falls keine Verteilung angegeben ist, empfiehlt die VDI/VDE/DGQ 2618 [^lit-002] die Annahme einer Rechteckverteilung. Ebenso wird bei Angabe des Zusatzes "max" von einer Rechteckverteilung ausgegangen. Ist eine Verteilung extra angegeben, so ist der Fall eindeutig. Die einzelnen Faktoren sind der folgenden Tabelle zu entnehmen [^lit-001]:

| Verteilung | Faktor |
| :--- | ---: |
| Normalverteilung | 1,00 |
| Dreiecksverteilung | 0,41 |
| Rechteckverteilung | 0,58 |
| U-Verteilung | 0,71 |

### Allgemeine Daten

#### Sensor

Aus [Sensorart](#sensorart) und [Messgröße](#messgröße) setzt sich automatisch die Spalte Sensor zusammen. Sie zeigt an, um welches Messgerät und gleichzeitig welche Messgröße es sich handelt.
Stehen drei Ausrufezeichen vor dem Sensor, so bedeutet dies, dass die Angaben nicht vollständig oder dem Datenblatt nicht entnehmbar sind. Zusätzlich sind diese Sensoren in der Farbe Rot gekennzeichnet.
Vollständig bedeutet hier, dass mindestens ein berechenbarer Fehlerwert angegeben ist.

#### Hersteller

Name der Herstellerfirma

#### Typenbezeichnung

Der von der Herstellerfirma vergebene Name des Sensors

#### Sensorart

Kategorie des Sensors aus Sicht des Messdatenerfassungssystems

#### Messgröße

Manche Sensoren können mehrere Größen messen, und damit je nach Messgröße auch unterschiedliche Unsicherheiten aufweisen.
Deshalb erfolgt hier eine Unterscheidung nach Messgröße.

#### Einheit

In der hier angegebenen Einheit werden alle folgenden Werte angegeben, sofern nicht in Zeile 2 anders vermerkt (zum Beispiel bei der Temperatur).
Diese Einheit bezieht sich sowohl auf die Full Scale FS, als auch auf die absoluten Messfehler, als auch auf die [Gesamtunsicherheit](#berechnung-gesamtunsicherheit).

#### Full Scale FS

Dies entspricht dem im Datenblatt angegebenen höchstmöglichen Messwert.

#### Tmin \[°C\]

Hier wird die minimale von dem Messgerät unterstütze Arbeitstemperatur eingetragen.
Oft wird der Bereich zwischen T<sub>min</sub> und T<sub>max</sub> Nenntemperaturbereich genannt.

#### Tmax \[°C\]

Analog zu Tmin steht hier die maximale Arbeitstemperatur

#### Tref \[°C\]

Ist bereits im Datenblatt eine Referenztemperatur gegeben, so wird diese hier eingetragen.

#### Pmin \[bar\]

Hier wird der minimale Arbeitsdruck eingetragen.

#### Pmax \[bar\]

Hier wird der maximale Arbeitsdruck eingetragen.

### Absolute Fehler

#### Anfangsfehler

Ein Beispiel für den Anfangsfehler gibt eine optische Partikelmessung.
Damit die Partikel überhaupt detektiert werden, müssen sie eine gewisse Größe besitzen.
Diese Schwelle wird als Anfangsfehler bezeichnet.

#### Auflösung

Die Auflösung bezieht sich auf die Auflösung des Messgeräts.
Gibt eine Waage zum Beispiel das Gewicht in kg bis auf 3 Nachkommastellen an, so ergibt sich ein Auflösungsfehler von 0,001 kg.
Die Auflösung wird seltener auch in Abhängigkeit vom Messwert oder vom Messbereich angegeben.

#### Genauigkeit

Einige Hersteller geben anstelle von differenzierten Fehlerquellen grundsätzlich eine Gesamtgenauigkeit an.
Diese wird, sofern sie absolut ist, hier eingetragen.
Darüber hinaus wird oft auch eine Genauigkeit in Abhängigkeit der Full Scale oder vom Messwert angegeben.

#### Kalibrierung

Hier wird die Kalibrierungenauigkeit eingetragen.
Wird zum Beispiel eine Waage mit einem Gewichtsstück geeicht, so bezieht sich die Kalibrierungenauigkeit auf den Messfehler, mit dem die Masse des Gewichtsstücks bestimmt wurde.

#### Linearitätsabweichung

Die Linearitätsabweichung ist die maximale Abweichung zwischen tatsächlichem Messwert und der vom Messgerät als Gerade angenäherten Kennlinie [^lit-001].
Sie wird entweder als Absolutwert (maximale Abweichung über den gesamten Messbereich) oder in Abhängigkeit der Full Scale angegeben.

#### Magnetischer Offset

Hierbei handelt es sich um einen aufgrund der Schaltung und eingesetzten Bauteile unvermeidbaren systematischen Fehler, der bei Spannungs- und Strommessgeräten auftritt.

#### Offset

Siehe [Magnetischer Offset](#magnetischer-offset).
Er tritt bei Spannungs- und Strommessern auf.

#### Rauschen

Hiermit ist eine Störgröße gemeint, die von der Frequenz des Messgeräts abhängig ist.

#### Temperaturabweichung

Durch einen Temperatureinfluss auf die einzelnen Bauteile des Messgeräts kann sich die Kennlinie verschieben.
Ausschlaggebend ist hier die Temperatur an der Messstelle.

Die Temperaturabweichung kommt sowohl absolut als auch in Abhängigkeit der Full Scale und des Messwertes vor.

#### Temperaturabweichung/10 K

Siehe [Temperaturabweichung](#temperaturabweichung).
Diese Temperaturabweichung ist auf eine Temperaturänderung von 10 K bezogen.
Manche Hersteller bevorzugen diese Angabe, da sie akkurater ist.
In den Datenblättern ist immer angegeben, ob sich die Angabe auf den gesamten Temperaturbereich bezieht oder je nur auf einen Teil.

#### Wiederholgenauigkeit

Mit Wiederholgenauigkeit ist die Eigenschaft gemeint, dass bei Durchführung mehrerer Messungen nicht immer dasselbe Ergebnis erzielt wird.
Voraussetzung hierfür sind Messungen bei ansonsten gleichen Bedingungen.

### Fehler in Abhängigkeit der Full Scale FS

#### Druckabhängigkeit/bar

Einige Messgeräte sind nicht nur temperatur-, sondern auch druckempfindlich.
Diese Anhängigkeit wird hier eingetragen.

#### Genauigkeit

Siehe absolute [Genauigkeit](#genauigkeit).

#### Kalibrierung

Siehe Angabe des absoluten [Kalibrationswertes](#kalibrierung).

#### Linearitätsabweichung

Siehe absolute [Linearitätsabweichung](#linearitätsabweichung).

#### Spannweite

"Spannweite" ist ein von manchen Herstellern benutzter Begriff für die Genauigkeit.

#### Temperaturabweichung

Siehe Angabe der absoluten [Temperaturabweichung](#temperaturabweichung).

#### Temperaturabweichung/10 K

Siehe Angabe der absoluten [Temperaturabweichung/10 K](#temperaturabweichung-10-k).
Hier wird der Wert auf die Full Scale bezogen.

#### Hysterese

Die Hysterese bezeichnet die Differenz der Ausgangssignale zwischen steigender und fallender Belastung.

Öfters werden auch die Linearitätsabweichung und Hysterese zusammengefasst, in diesem Fall reicht es, den Wert in einer der beiden Spalten einzutragen.

### Fehler in Abhängigkeit des Messwertes Y

#### Auflösung

Siehe Angabe der absoluten [Auflösung](#auflösung).

#### Genauigkeit

Siehe Angabe der absoluten [Genauigkeit](#genauigkeit).

#### Sensitivitätsfehler

Sensitivität bezeichnet die notwendige Änderung der Eingangsparameter, um eine feststellbare Ausgangssignaländerung zu erzeugen.

#### Spannungskoeffizient

:construction: TODO - ERGÄNZEN

#### Temperaturabweichung

Siehe Angabe der absoluten [Temperaturabweichung](#temperaturabweichung).

#### Temperaturabweichung/10K

Siehe Angabe der absoluten [Temperaturabweichung/10 K](#temperaturabweichung-10-k).
Hier wird der Wert auf den Messwert bezogen.

### Fehler bezogen auf den Messbereich

#### Auflösung

Siehe Angabe der absoluten [Auflösung](#auflösung).

## Arbeitsblatt "Datenblätter Messverstärker"

In dem Blatt "Datenblätter Messverstärker" werden alle bekannten Daten zu den Messverstärkerarten festgehalten.
Diese sind aufgeteilt in allgemeine Angaben, absolute Fehler, Fehler in Abhängigkeit von FS, und Fehler in Abhängigkeit von Y.
Die Daten werden dabei in einer sich selbst erweiternden Tabelle festgehalten.

Auch hierbei müssen nicht alle Felder zwingend ausgefüllt werden.
Wenn ein Temperaturfehler vorliegt, dann sollte auch die Arbeitstemperaturspanne, also die Werte T<sub>min</sub> und T<sub>max</sub> angegeben werden, sofern nicht die wirkliche Arbeitstemperatur und die Referenztemperatur des Messverstärkers bekannt sind.

Die Fehlerwerte werden nach dem selben Schema wie die Fehlerwerte für die Sensoren angegeben (s. [oben](#arbeitsblatt-datenblätter-sensor):

```math
\frac{\text{Wert} \cdot \text{Verteilungsfaktor}}{\text{Faktor der Erweiterung} \cdot \text{Halbbreite}}
```

### Allgemeine Daten

#### Messverstärker

Der Punkt "Messverstärker" setzt sich aus der [Messverstärkerart](#messverstärkerart) und der [Art](#art) zusammen.
Die Inhalte der Spalte können später im Dropdown-Menü im Arbeitsblatt "Berechnung" ausgewählt werden.

#### Messverstärkerart

Die Messverstärkerart bezieht sich auf die vom Hersteller angegebene Bezeichnung des Messverstärkers.

#### Art

Die Art bezieht sich auf die unterschiedlichen Möglichkeiten der Nutzung, die ein einzelner Messverstärker bietet.
So können z. B. die Eingänge eines QuantumX-Gerätes auf unterschiedliche Signalarten konfiguriert werden, die jeweils unterschiedliche Ungenauigkeitscharakteristika aufweisen.
Mit der Art ist die Konfiguration dieser Eingänge gemeint.

#### Tmin \[°C\]

Hier steht die minimale Temperatur des Nenntemperaturbereichs (s. Angabe des [absoluten Temperaturbereiches](#tmin-c) für Sensoren).

#### Tmax \[°C\]

Hier steht die maximale Temperatur des Nenntemperaturbereichs.

#### Tref \[°C\]

Ist bereits im Datenblatt eine Referenztemperatur angegeben, so wird diese hier eingetragen.

### Absolute Fehler

#### Rauschen

Hiermit ist eine Störgröße gemeint, die von der Frequenz des Messverstärkers abhängig ist.

:construction: TODO: Das muss besser erklärt werden.

#### Linearitätsabweichung

Die Linearitätsabweichung ist die maximale Abweichung zwischen tatsächlichem Messwert und der vom Messgerät als Gerade angenäherten Kennlinie [^lit-001].
Sie wird entweder als Absolutwert (maximale Abweichung über den gesamten Messbereich) oder in Abhängigkeit der Full Scale angegeben.

#### Nullpunktdrift/10 K

Die Nullpunktdrift ist die Änderung des Ausgangssignals des unbelasteten Aufnehmers infolge einer Temperaturänderung von 10 K.
Sie bewirkt eine Parallelverschiebung der Kennlinie.

#### Endwertdrift/10 K

Die Endwertdrift ist die Änderung des tatsächlichen Ausgangssignals infolge einer Temperaturänderung von 10 K.
Sie bewirkt eine Änderung der Steigung der Kennlinie.

#### Temperaturdrift/10 K

Manchmal wird die [Nullpunktdrift](#nullpunktdrift10-k) und die [Endwertdrift](#endwertdrift10-k) als eine Größe zusammengeschrieben.

#### Gesamtfehlergrenze

Die Gesamtfehlergrenze gibt den Gesamtfehler an. Sie wird oft anstelle einer Aufschlüsselung in die einzelnen Fehler verwendet.

### Fehler in Abhängigkeit der Full Scale in Prozent

#### Linearitätsabweichung

Die Linearitätsabweichung kann sowohl als absoluter Fehler als auch in Abhängigkeit des Messbereichsendwertes C angegeben werden.

#### Nullpunktdrift/10 K

Die Nullpunktdrift kann sowohl als absoluter Fehler als auch in Abhängigkeit des Messbereichsendwertes C angegeben werden.

### Fehler in Abhängigkeit von Y in Prozent

#### Endwertdrift /10K

Die Endwertdrift kann sowohl als absoluter Fehler als auch in Abhängigkeit des Messwertes Y angegeben werden.

#### Rauschen

Auch dieser Fehler kann sowohl als absoluter Fehler als auch in Abhängigkeit des Messwertes Y angegeben werden.

#### Temperaturdrift/10 K

Siehe absolute Angabe der [Temperaturdrift](#temperaturdrift10-k).

## Arbeitsblatt "Berechnung Messunsicherheit"

Im Blatt "Berechnung Messunsicherheit" findet die eigentliche Berechnung der Messunsicherheit statt.
Hierzu werden aus dem Arbeitsblatt "Messstellenliste" die zuvor ausgewählten Messstellen inklusive Sensorart und Messverstärker sowie die zugehörigen Informationen übernommen.
Zu den übernommenen Daten gehören die allgemeinen Daten sowohl der Sensoren als auch der Messverstärker, ferner die absoluten Fehler, Fehler in Abhängigkeit von FS, Fehler in Abhängigkeit von Y und Fehler bezogen auf den Messbereich.
Diese sind in der Gruppierung "Übernahme aus Datenblätter" wiederzufinden. Die Angaben der Sensorart, Messgröße und Einheit werden der Übersichtlichkeit halber am Anfang der Tabelle noch einmal aufgegriffen.

### Rote Felder

In den roten Feldern können Eintragungen bezogen auf einen spezifischen Sensor gemacht werden.
Es empfiehlt sich, die Felder möglichst vollständig auszufüllen.
Sollte aber für manche der Felder kein Wert vorliegen, so funktioniert die Berechnung trotzdem.

#### Messwert Y

Hier wird der aktuelle Messwert eingetragen, für den die Unsicherheitsberechnung durchgeführt werden soll.

#### Messbereich

Der Messbereich ist der gesamte Bereich, über den im Rahmen einer Messkampagne Messungen durchgeführt werden.

#### Arbeitstemperatur Messstelle \[°C\]

Mit Arbeitstemperatur ist die aktuelle Temperatur unmittelbar an der Messstelle gemeint.

#### Arbeitstemperatur Messverstärker \[°C\]

Hier wird die aktuelle Temperatur des Messverstärkers eingetragen.

#### Referenztemperatur Messstelle \[°C\]

Wurde die Kalibrierung nicht vom Hersteller durchgeführt, damit also auch keine Referenztemperatur vom Hersteller angegeben, so kann hier eine eigene Referenztemperatur angegeben werden.
Dies ist dann zum Beispiel die Umgebungstemperatur, die bei Kalibrierung unmittelbar vor der Messung vorlag.

#### Referenztemperatur Messverstärker \[°C\]

Analog zur [Referenztemperatur der Messstelle](#referenztemperatur-messstelle-c) wird hier die Referenztemperatur des Messverstärkers eingetragen, sofern sie denn vorliegt.

#### Arbeitsdruck \[bar\]

Analog zur [Arbeitstemperatur](#arbeitstemperatur-messstelle-c) steht hier der aktuelle Druck unmittelbar an der Messstelle.

#### Referenzdruck \[bar\]

Analog zur [Referenztemperatur der Messstelle](#referenztemperatur-messstelle-c) steht hier der Referenzdruck.

### Berechnung

Die Berechnung erfolgt in mehreren Schritten.
Zuerst werden die Einzelunsicherheiten berechnet und anschließend aus diesen die Gesamtunsicherheit.
Die Berechnungsvorschrift ist dabei die folgende:

```math
U_{ges} = \sqrt{\sum_{i}{u_i^2}
```

Die Vorgehensweise ist wie folgt:

* Berechnung der Einzelunsicherheiten
* Quadrieren der Einzelunsicherheiten
* Bilden der Summe der quadrierten Einzelunsicherheiten bezogen auf die jeweilige Abhängigkeit
* Berechnung der Gesamtunsicherheit

Die Berechnung erfolgt dabei in der Gruppierung "Berechnung Sensor" für den Sensor, und analog in der Gruppierung "Berechnung Messverstärker" für den Messverstärker.

#### Berechnung der absoluten Fehler

Bei den absoluten Fehlern muss nichts berechnet werden, da sie, im Gegensatz zu den Fehlern in Abhängigkeit von FS und Y, nicht in Prozent angegeben sind.
Es werden also einfach die Werte aus den Datenblättern übernommen.
Einzige Ausnahme ist die Berechnung der absoluten Temperaturfehler, dazu [unten](#berechnung-temperaturfehler) mehr.

#### Berechnung Fehler in Abhängigkeit von der Full Scale FS

Die Berechnung der Fehler in Abhängigkeit von C erfolgt folgendermaßen:

```math
\frac{\text{Wert} \cdot FS}{100}
```

Da sich die in den Datenblättern angegebenen Werte auf FS in Prozent beziehen, muss dies mit eingerechnet werden.

#### Berechnung Fehler in Abhängigkeit von Y

Die Berechnung der Fehler in Abhängigkeit von Y erfolgt ähnlich zu der Berechnung der Fehler in Abhängigkeit von C:

```math
\frac{\text{Wert}}{100}
```

Der aktuelle Messwert Y wird hier allerdings nicht mit eingerechnet, da er nicht immer gegeben ist.
Die Einberechnung des Messwertes Y erfolgt daher erst bei der Berechnung der Gesamtunsicherheit.
Die Tatsache, dass die Angaben Prozentwerte sind, wird berücksichtigt.

#### Berechnung Fehler bezogen auf den Messbereich

Die Berechnung erfolgt analog zu den Fehlern in Abhängigkeit von Y:

```math
\frac{\text{Wert}}{100}
```

Auch hier wird der Messbereich erst wieder in der Berechnung der Gesamtunsicherheit mit einberechnet.

Die Fehler bezogen auf den Messbereich treten nur für den Sensor auf. Diese Berechnung wird daher für den Messverstärker nicht durchgeführt.

#### Rundung der summierten Fehler

Für die Rundung der summierten Fehler wird in zwei Gruppen unterschieden:

Die erste Gruppe bezieht sich auf die __Summe der absoluten Fehler__ $`\Sigma_{abs}`$ sowie der __Fehler in Abhängigkeit von FS__ $`\Sigma_C`$.

:construction: TODO: "XXX" im nachfolgenden Absatz ersetzen.

Die Rundung erfolgt nach der Anzahl der Vorkommastellen der Full Scale FS.
Durch die in XXX angegebene Verteilung ist gewährleistet, dass der Fehler zumindest numerisch bis auf 0,001 % genau berechnet wird.

| FS 1-stellig | Runden auf 5 NKS |
| :--- | :--- |
| FS 2-stellig | Runden auf 4 NKS |
| FS 3-stellig | Runden auf 3 NKS |
| FS 4-stellig | Runden auf 2 NKS |
| FS 5-stellig | Runden auf 1 NKS |

Die zweite Gruppe bezieht sich auf die __Summe der Fehler in Abhängigkeit von Y__ und der __Fehler in Abhängigkeit des Messbereiches__.
Da hier in der Summierung der Wert des Y<sup>2</sup> bzw ΔY<sup>2</sup> noch nicht mit eingerechnet wird, sondern dies erst in der Endsumme geschieht, muss hier entsprechend sorgfältiger gerundet werden, da bei quadrierten 5-stelligen Zahlen bereits sehr kleine Fehler einen großen Einfluss haben.
Um die gleiche numerische Genauigkeit von 0,001 % zu gewährleisten, muss dann wie folgt gerundet werden:

| FS 1-stellig | Runden auf 6 NKS |
| :--- | :--- |
| FS 2-stellig | Runden auf 7 NKS |
| FS 3-stellig | Runden auf 8 NKS |
| FS 4-stellig | Runden auf 9 NKS |
| FS 5-stellig | Runden auf 10 NKS |

Die Art der Rundung ist dabei immer von FS abhängig, da sowohl Y als auch ΔY als höchsten Wert maximal FS annehmen können.

#### Berechnung Temperaturfehler

Bei den Temperaturfehlern ist die Berechnung ein wenig komplizierter.
Da die Temperaturfehler in Bezug auf die Temperaturdifferenz angegeben werden, müssen diese Differenzen erst errechnet werden.
Dazu wird folgendermaßen vorgegangen:

![Schema zur Berechnung der Temperaturdifferenz](media/schema-temperaturdifferenz.png "Schema zur Berechnung der Temperaturdifferenz")

Dieses Schema ergibt sich aus der Tatsache, dass es nicht immer möglich ist, eine Arbeitstemperatur anzugeben, und dass ferner auch manchmal die Referenztemperaturen unbekannt sind.

Wird dann pro 10 K gerechnet, so ergibt sich daraus als Wert, mit dem je nach Fehlerabhängigkeit weitergerechnet wird:

```math
\text{Wert}_{\text{neu}} = \text{Wert}_{\text{alt}} \cdot \Delta T/10
```

Allerdings beziehen sich nicht alle Angaben auf 10 K, manchmal werden sie auch allgemein angegeben (s. [Temperaturabweichung](#temperaturabweichung)).
Dann muss nicht mit Differenzen gearbeitet werden, da der Hersteller selbst schon die maximal mögliche Abweichung unabhängig von der Temperatur angibt.

#### Berechnung Druckfehler

Die Berechnung des Druckfehlers verläuft ähnlich zu der des Temperaturfehlers:

![Schema zur Berechnung der Druckdifferenz](media/schema-druckdifferenz.png "Schema zur Berechnung der Druckdifferenz")

Auch hier gilt dann wieder (hier allerdings pro bar):

```math
\text{Wert}_{\text{neu}} = \text{Wert}_{\text{alt}} \cdot \Delta p
```

### Berechnung Gesamtunsicherheit

Die Gesamtunsicherheit berechnet sich folgendermaßen:

```math
U_{\text{ges}} = \ u_{\text{ges}} = \sqrt{\sum_i u_{i}^2}
```

Der im Programm berechnete Unsicherheitswert entspricht einem Grad des Vertrauens von 68,27 %, also einem Erweiterungsfaktor von k=1 ("Standard-Messunsicherheit").
Um höhere Grade des Vertrauens zu erreichen, kann folgende Umrechnungstabelle benutzt werden [^lit-001]:

| Grad des Vertrauens \[%\] | Erweiterungsfaktor |
| ---: | ---: |
| 68,27 | 1,000 |
| 90,00 | 1,645 |
| 95,00 | 1,960 |
| 95,45 | 2,000 |
| 99,00 | 2,576 |
| 99,73 | 3,000 |

Nun besteht aber das Problem, dass die Werte des aktuellen Messwerts und des Messbereichs nicht immer vorliegen.
Daher wird folgendermaßen vorgegangen:

Liegen beide Werte, also Y und ΔY vor, so wird die Gesamtunsicherheit berechnet.
Liegt einer der beiden Werte nicht vor, aber es sind auch keine Fehler in Abhängigkeit von diesem Wert angegeben, so wird die Gesamtunsicherheit auch berechnet.

Sofern die Werte nicht vorliegen, wird das Ergebnis nicht als Wert, sondern als "Verkettung" (als Funktion des Messwertes) ausgegeben.
Die nachstehende Grafik zeigt das Schema der Vorgehensweise.
Dabei stehen in der rechten Spalte die Ausgaben, schwarzgeschriebene Ausdrücke werden berechnet, graue Ausdrücke als Verkettung von Buchstaben und Zeichen ausgegeben.

![Schema der Ausgabe der Gesamtunsicherheit](media/schema-ausgabe-gesamtunsicherheit.png "Schema der Ausgabe der Gesamtunsicherheit")

Dabei beziehen sich die einzelnen Summen jeweils auf die Summe der Fehler sowohl vom Sensor als auch vom Messverstärker.
Also:

:construction: TODO: In Grafik und Formeln anpassen: $`\sum u_{abs,Sensor}`$ etc. anstelle von $`\sum \text{abs}_\text{Sensor}`$

```math
\sum \text{abs} = \sum {\text{abs}_{\text{Sensor}} + \sum \text{abs}_{\text{Messverstärker}}}
```

```math
\sum \text{FS} = \sum \text{FS}_{\text{Sensor}} + \sum \text{FS}_{\text{Messverstärker}}
```

```math
\sum Y = \sum Y_{\text{Sensor}} + \sum Y_{\text{Messverstärker}}
```

:construction: TODO: Folgenden Satz korrigieren

Sowie für den Wert bezogen auf den Messbereich (diese treten nur für die Sensoren, nicht aber für die Messverstärker auf):

```math
\sum \Delta Y = \sum \Delta Y_{\text{Sensor}}
```

Die Gesamtunsicherheit wird nach dem selben Schema gerundet wie die absoluten Fehler, also:

| FS 1-stellig | Runden auf 5 NKS |
| :--- | :--- |
| FS 2-stellig | Runden auf 4 NKS |
| FS 3-stellig | Runden auf 3 NKS |
| FS 4-stellig | Runden auf 2 NKS |
| FS 5-stellig | Runden auf 1 NKS |


## Referenzen

[^lit-001]: Hesse, Thomas; Wegener, Georg; Bäumel, Angela: Der praktische Weg zur Ermittlung der Messunsicherheit, Skript zum Seminar der HBM Academy

[^lit-002]: VDI/VDE/DGQ (Verein Deutscher Ingenieure und andere): VDI/VDE/DGQ-Richtlinie 2618, Prüfmittelüberwachung - Anweisungen zur Überwachung von Messmitteln für geometrische Größen - Messunsicherheit, 2005

