# -*- coding: utf-8 -*-
"""
Erzeugen von individualisierten Vorlagen für die Dokumentation von Sensorik auf
Basis einer gegebenen Messstellenliste.

Funktion
--------

Das Skript benötigt als Eingabe eine Messstellenliste (Excel-Tabelle mit einem
Tabellenblatt "Messstellenliste", die alle zu dokumentierenden Signale enthält.

Folgende Spalten werden ausgelesen:

* `Messstellenname`: Name der Messstelle
* `Sensorart`: Art des Sensors
* `Hersteller`: Hersteller des Sensors
* `Typenbezeichnung`: Typbezeichnung des Sensors
* `Messgröße`: Zu erfassende physikalische Größe
* `Einheit`: Physikalische Einheit der Messgröße
* `Signalart`: Art des Messsignals								

Über Benutzereingaben werden einmalig pro Aufruf Projektname und Name der
verantwortlichen Person erfasst.

Der Skriptaufruf erzeugt auf Basis der Vorlage
`Vorlage_Messstellendokumentation.docx` für jeden Eintrag in der Messstellenliste
ein vorausgefülltes Word-Dokument zur Ergänzung mit weiteren nötigen
Informationen. Das Python-Skript und die Vorlage müssen dazu auf dem selben
Arbeitsverzeichnis liegen.
Die Dokumente werden im Verzeichnis ./output/ abgelegt. Dieses Verzeichnis wird
bei jedem Skriptaufruf ohne Warnung überschrieben.

Die Word-Vorlage enthält MergeFields, die beim Skriptaufruf mit den
entsprechenden Informationen aus der Messstellenliste ersetzt werden.

Verwendung
----------

`python seriendruck.py <pfad/zur/messstellenliste.xlsx>`

Systemvoraussetzungen
---------------------

Python 3.7 (z. B. in Anaconda) mit folgenden Paketen:
* argparse
* docx-mailmerge
* pandas
"""

import os
from shutil import rmtree
from argparse import ArgumentParser
from pandas import read_excel
from mailmerge import MailMerge


## KONFIGURATION ##############################################################

# Zuordnung der Feldnamen in der Word-Vorlage zu den Spaltennamen der Excel-
# Messstellenliste.
#   keys: Feldnamen in der Vorlage
#   values: Spaltennamen in Messstellenliste
FELDZUORDNUNG_ALLGEMEIN = {  # Werte gelten für alle Dokumente, aus User-Input
    'user_projektname': 'Projekt',
    'user_name': 'Name'}

EINGABEAUFFORDERUNG_ALLGEMEIN = {
    'user_projektname': 'Projektnamen eingeben: ',
    'user_name': 'Verantwortlicher Mitarbeiter: '}

FELDZUORDNUNG_SPEZIELL = {  # Erhalte Werte aus Excel-Messstellenliste
    'user_messstellenname': 'Messstellenname',
    'user_sensorart': 'Sensorart',
    'user_hersteller': 'Hersteller',
    'user_sensortyp': 'Typenbezeichnung',
    'user_messgroesse': 'Messgröße',
    'user_einheit': 'Einheit',
    'user_signalart': 'Signalart'}

# Word-Vorlage (Pfad relativ zum Arbeitsverzeichnis)
DOCX_VORLAGE = '../Vorlagen/Vorlage_Messstellendokumentation.docx'


## FUNKTIONEN #################################################################

def main():

    # Argumente parsen
    parser = ArgumentParser()
    parser.add_argument('messstellenliste_pfad',
                        help='Pfad zur Messstellenliste')
    args = parser.parse_args()
    messstellenliste_pfad = args.messstellenliste_pfad

    # Messstellenliste laden
    messstellenliste = read_excel(messstellenliste_pfad,
                                  sheet_name='Messstellenliste',
                                  header=0,
                                  usecols=FELDZUORDNUNG_SPEZIELL.values())

    # Word-Template laden
    sensordoku_template = os.path.join(os.getcwd(), DOCX_VORLAGE)

    # Benutzereingaben
    user_input = dict.fromkeys(FELDZUORDNUNG_ALLGEMEIN.keys())
    for k in user_input.keys():
        user_input[k] = input(EINGABEAUFFORDERUNG_ALLGEMEIN[k])

    # Vorlagen erzeugen
    generiere_vorlagen(messstellenliste, sensordoku_template,
                       user_input=user_input)


def generiere_vorlagen(messstellenliste, sensordoku_template,
                       user_input=None, output=True):
    """
    Iteriere über <messstellenliste> und erzeuge ein Word-Dokument für jede
    Messstelle auf Basis der Vorlage <sensordoku_template>.
    """

    # Lösche evtl. vorhandenes Output-Verzeichnis, erzeuge neues
    output_pfad = os.path.join(os.getcwd(), 'output')
    if os.path.isdir(output_pfad):
        rmtree(output_pfad)
    os.mkdir(output_pfad)

    zaehler_sensor = 1
    for index, row in messstellenliste.iterrows():
        if output:
            print('  Sensor {} von {}: {} ...'
                  .format(zaehler_sensor, len(messstellenliste),
                          row['Messstellenname']), end='')

        merge_felder = dict.fromkeys(FELDZUORDNUNG_SPEZIELL.keys())
        for k in merge_felder.keys():
            merge_felder[k] = row[FELDZUORDNUNG_SPEZIELL[k]]

        # Kombinieren mit Usereingabe
        merge_felder.update(user_input)

        with MailMerge(sensordoku_template) as zieldokument:
            zieldokument.merge(**merge_felder)
            zieldokument.write(os.path.join( \
                output_pfad, '{}.docx'.format(row['Messstellenname'])))

        if output:
            print('Ok')

        zaehler_sensor += 1


## TREIBER ####################################################################

if __name__ == '__main__':
    main()
