# Vorlagen und Tools für die Dokumentation von Messdaten

Dieses Repository enthält Vorlagen, die für die Dokumentation von Mess- und Simulationsdaten vorbereitet wurden. Außerdem ist ein Python-Skript enthalten, mit dem vor-ausgefüllte Messstellendokumentationen aus einer Messstellenliste erzeugt werden können.

## Messstellenliste

`Messstellenliste.xlsx` wird verwendet, um eine übersichtliche Liste aller Messstellen eines Projektes zu verwalten. Einzutragen sind folgende Informationen:

* Eingesetzte Sensoren: Typ, Hersteller, Messgröße, Informationen zur Messunsicherheit lt. Datenblatt
* Messstellen: Zugeordnete Sensoren und Messverstärker

Sind die nötigen Informationen aus den Sensordatenblättern eingetragen, so berechnet die Liste die Messunsicherheit (= das zu erwartende Konfidenzintervall für die Messwerte) für jede einzelne Messstelle nach Methode B entsprechend GUM [^lit-001] ([Guide to the Expression of Uncertainty Measurement](https://de.wikipedia.org/wiki/GUM_(Norm))). Eine detaillierte Dokumentation der Funktionalitäten der Messstellenliste ist [hier](./Messstellenliste/Bedienungsanleitung.md) zu finden.

## Vorlagen

Dieses Verzeichnis enthält Vorlagen für Messstellen- und Testreihendokumentationen. Die Vorlage für Messstellendokumentationen wird bewusst nicht als Word-Vorlage (dotx) gepflegt, um die [Seriendruck-Funktion](#seriendruck-messstellendokumentation) über Python zu ermöglichen. Weiterhin enthält das Verzeichnis jeweils ein Beispiel für die Dokumentation einer Messstelle bzw. einer Testreihe.

## Seriendruck Messstellendokumentation

Erzeugen von individualisierten Vorlagen für die Dokumentation von Sensorik auf
Basis einer gegebenen Messstellenliste.

### Funktion
--------

Das Skript benötigt als Eingabe eine Messstellenliste (Excel-Tabelle mit einem
Tabellenblatt "Messstellenliste", die alle zu dokumentierenden Signale enthält.

Folgende Spalten werden ausgelesen:

* `Messstellenname`: Name der Messstelle
* `Sensorart`: Art des Sensors
* `Hersteller`: Hersteller des Sensors
* `Typenbezeichnung`: Typbezeichnung des Sensors
* `Messgröße`: Zu erfassende physikalische Größe
* `Einheit`: Physikalische Einheit der Messgröße
* `Signalart`: Art des Messsignals								

Über Benutzereingaben werden einmalig pro Aufruf Projektname und Name der
verantwortlichen Person erfasst.

Der Skriptaufruf erzeugt auf Basis der Vorlage
`Vorlage_Messstellendokumentation.docx` für jeden Eintrag in der Messstellenliste
ein vorausgefülltes Word-Dokument zur Ergänzung mit weiteren nötigen
Informationen.
Der Pfad der Word-Vorlage muss im Python-Skript konfiguriert werden.
Arbeitsverzeichnis liegen.
Die Dokumente werden im Verzeichnis ./output/ abgelegt.
__Dieses Verzeichnis wird bei jedem Skriptaufruf ohne Warnung überschrieben.__

Die Word-Vorlage enthält MergeFields, die beim Skriptaufruf mit den
entsprechenden Informationen aus der Messstellenliste ersetzt werden.

### Verwendung

Der Aufruf erfolgt über die Kommandozeile:
`python seriendruck.py <pfad/zur/messstellenliste.xlsx>`

### Systemvoraussetzungen
---------------------

Python 3.7 (z. B. in Anaconda) mit folgenden Paketen:
* argparse
* docx-mailmerge
* pandas

## Referenzen

[^lit-001]: Internationale Organisation für Normung, ISO/IEC Guide 98-3:2008: Uncertainty of measurement, Part 3: Guide to the expression of uncertainty in measurement. ISO, Genf 2008, ISBN 92-67-10188-9.

